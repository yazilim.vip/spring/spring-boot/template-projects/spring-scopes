package vip.yazilim.springboot.scopes.bean;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

/**
 *
 * @author Emre Sen - Aug 9, 2018
 */
@Component
//@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@SessionScope
public class SessionCounter extends GlobalCounter {

}
