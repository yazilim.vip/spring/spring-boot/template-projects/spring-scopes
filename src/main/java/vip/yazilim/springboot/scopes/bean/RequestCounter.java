package vip.yazilim.springboot.scopes.bean;


import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

/**
 *
 * @author Emre Sen - Aug 9, 2018
 */
@Component
//@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequestScope
public class RequestCounter  extends GlobalCounter {

}
