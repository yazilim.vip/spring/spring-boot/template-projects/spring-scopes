package vip.yazilim.springboot.scopes.controller;

import vip.yazilim.springboot.scopes.bean.PrototypeCounter;
import vip.yazilim.springboot.scopes.bean.RequestCounter;
import vip.yazilim.springboot.scopes.bean.SessionCounter;
import vip.yazilim.springboot.scopes.bean.SingletonCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    private PrototypeCounter prototypeCounter;

    @Autowired
    private PrototypeCounter prototypeCounter2;

    @Autowired
    private RequestCounter requestCounter;

    @Autowired
    private SessionCounter sessionCounter;

    @Autowired
    private SingletonCounter singletonCounter;

    @Autowired
    private SingletonCounter singletonCounter2;

    @RequestMapping("/")
    public String home(Model model) {
        model.addAttribute("prototype_var", prototypeCounter.getCounter());
        model.addAttribute("prototype2_var", prototypeCounter2.getCounter());
        model.addAttribute("request_var", requestCounter.getCounter());
        model.addAttribute("session_var", sessionCounter.getCounter());
        model.addAttribute("singleton_var", singletonCounter.getCounter());
        model.addAttribute("singleton2_var", singletonCounter2.getCounter());
        return "home";
    }

    @RequestMapping("/prototype")
    public String prototype_var(Model model) {
        prototypeCounter.increment();
        home(model);
        return "home";
    }

    @RequestMapping("/prototype2")
    public String prototype2_var(Model model) {
        prototypeCounter2.increment();
        home(model);
        return "home";
    }

    @RequestMapping("/request")
    public String request_var(Model model) {
        requestCounter.increment();
        home(model);
        return "home";
    }

    @RequestMapping("/session")
    public String session_var(Model model) {
        sessionCounter.increment();
        home(model);
        return "home";
    }

    @RequestMapping("/singleton")
    public String singleton_var(Model model) {
        singletonCounter.increment();
        home(model);
        return "home";
    }

    @RequestMapping("/singleton2")
    public String singleton2_var(Model model) {
        singletonCounter2.increment();
        home(model);
        return "home";
    }

}
